const WebSocket = require('ws');

function socketModule(server) {
    
   const wss = new WebSocket.Server({ server });
    
   wss.on('connection', function connection(ws, req) {
    
     ws.on('message', function incoming(message) {
       console.log('received: %s', message);
       ws.send(JSON.stringify({echo: message}));
     });
    
     const timer = setInterval(function(){
       ws.send(JSON.stringify(process.memoryUsage()),function(error) {
      
       });
     }, 100);
    
     console.log('Client connected');
   
     ws.on('close', function(){
       console.log('Client Disconnected');
       clearInterval(timer);
     });
     
     ws.on('error', () => console.log('errored'));  
   });   
};

 exports.Socket = socketModule;