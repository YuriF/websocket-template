
'use strict';

const express = require('express');
const http = require('http');
const app = express();
 
const PORT = process.env.PORT || 5000;

app.use(express.static(__dirname+'/public')); 
const server = http.createServer(app);
server.listen(PORT, () => console.log(`Listening on ${ PORT }`));
require('./wssmodule').Socket( server );